import Vue from "vue";
import App from "./App.vue";
// eslint-disable-next-line
import sassStyles from "./styles/styles.module.scss";

Vue.config.productionTip = false;

new Vue({
  render: h => h(App)
}).$mount("#app");
